# Welcome to Intermediate JavaScript Class!

## Instructor

Eric Greene

## Schedule

Class:

- Monday - Friday: 8am EST to 12:30pm EST

Breaks:

- Morning Break #1: 9:30am to 9:40am
- Morning Break #2: 11:00am to 11:10am

## Course Outline

- Day 1 - Types, Variables, Functions, Objects, Arrays
- Day 2 - Classes & Prototype Inheritance
- Day 3 - Asynchronous Programming (Part 1)
- Day 4 - Asynchronous Programming (Part 2)
- Day 5 - Error Handling, Regular Expressions (JavaScript API), Remaining Topics

## Links

### Instructor's Resources

- [Accelebrate, Inc.](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)
