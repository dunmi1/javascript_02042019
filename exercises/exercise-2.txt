Exercise 2

1. Earlier we had a shopping cart like this, and we coded a function to calculate the total.

Example:

const cart = [
  { item: 'bread', price: 2.34, qty: 3 },
  { item: 'eggs', price: 4.00, qty: 1 },
  { item: 'milk', price: 4.50, qty: 2 },
];

const total = calcTotal(cart);

// outputs: 20.02
console.log(total);

2. Create the code needed to implement the following API...

const cart = new ShoppingCart();

const item1 = new Item('bread', 2.34, 3);
cart.addItem(item1);

cart.addItem(new Item('eggs', 4, 1));
cart.addItem(new Item('milk', 4.5, 2));

console.log(cart.total());

As part of your implementation... Add a function to the Item class which returns the subtotal for the item (price * qty). 